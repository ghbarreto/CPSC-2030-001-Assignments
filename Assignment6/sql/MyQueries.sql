-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 29, 2018 at 03:46 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MyQueries`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `pokemon_all` ()  NO SQL
Select Name, TypeName
From Pokemon$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pokemon_popular` ()  NO SQL
SELECT Name, TypeName
FROM Pokemon
WHERE Name LIKE 'Mewtwo'$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Effectiveness`
--

CREATE TABLE `Effectiveness` (
  `TypeName` varchar(25) NOT NULL,
  `StrongAgainst` varchar(150) NOT NULL,
  `ResistantTo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Effectiveness`
--

INSERT INTO `Effectiveness` (`TypeName`, `StrongAgainst`, `ResistantTo`) VALUES
('Normal', '--', 'Ghost'),
('Fighting', 'Normal, Rock, Steel, Ice, Dark', 'Rock, Bug, Dark'),
('Flying', 'Fighting, Bug, Grass', 'Fighting, Ground, Bug, Grass'),
('Poison', 'Grass, Fairy', 'Fighting, Poison, Grass, Fairy'),
('Ground', 'Poison, Rock, Steel, Fire, Electric', 'Poison, Rock, Electric'),
('Rock', 'Flying, Bug, Fire, Ice', 'Fighting, Ground, Grass'),
('Bug', 'Grass, Psychic, Dark', 'Normal, Fighting, Poison, Bug'),
('Ghost', 'Ghost, Psychic', 'Normal, Fighting, Poison, Bug'),
('Steel', 'Rock, Ice, Fairy', 'Normal, Flying, Poison, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy'),
('Fire', 'Bug, Steel, Grass, Ice', 'Bug, Steel, Fire, Grass, Ice'),
('Water', 'Ground, Rock, Fire', 'Steel, Fire, Water, Ice'),
('Grass', 'Ground, Rock, Water', 'Ground, Water, Grass, Electric'),
('Electric', 'Flying, Water', 'Flying, Steel, Electric'),
('Psychic', 'Fighting, Poison', 'Fighting, Psychic'),
('Ice', 'Flying, Ground, Grass, Dragon', 'Ice'),
('Dragon', 'Dragon', 'Fire, Water, Grass, Electric'),
('Fairy', 'Fighting, Dragon, Dark', 'Fighting, Bug, Dragon, Dark'),
('Dark', 'Ghost, Psychic', 'Ghost, Psychic, Dark');

-- --------------------------------------------------------

--
-- Table structure for table `Pokemon`
--

CREATE TABLE `Pokemon` (
  `NatID` int(11) NOT NULL,
  `HOENN` varchar(5) DEFAULT NULL,
  `Name` varchar(25) NOT NULL,
  `TypeName` varchar(25) NOT NULL,
  `HP` int(11) NOT NULL,
  `Attack` int(11) NOT NULL,
  `Defense` int(11) NOT NULL,
  `SAT` int(11) NOT NULL,
  `SDF` int(11) NOT NULL,
  `Speed` int(11) NOT NULL,
  `BaseStat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Pokemon`
--

INSERT INTO `Pokemon` (`NatID`, `HOENN`, `Name`, `TypeName`, `HP`, `Attack`, `Defense`, `SAT`, `SDF`, `Speed`, `BaseStat`) VALUES
(63, '40', 'Abra', 'Psychic', 25, 20, 15, 105, 55, 90, 310),
(142, '-', 'Aerodactyl', 'Rock Flying', 80, 105, 65, 60, 75, 130, 515),
(65, '42', 'Alakazam', 'Psychic', 55, 50, 45, 135, 95, 120, 500),
(24, '-', 'Arbok', 'Psychic', 60, 85, 69, 65, 79, 80, 438),
(59, '-', 'Arcanine', 'Fire', 90, 110, 80, 100, 80, 95, 555),
(144, '-', 'Articuno', 'Ice Flying', 90, 85, 100, 95, 125, 85, 580),
(15, '-', 'Beedrill', 'Bug, Poison', 65, 90, 40, 45, 80, 75, 395),
(69, '-', 'Bellsprout', 'Grass, Poison', 50, 75, 35, 70, 30, 40, 300),
(9, '-', 'Blastoise', 'Water', 79, 83, 100, 85, 105, 78, 530),
(1, '-', 'Bulbasaur', 'Grass, Poison', 45, 49, 49, 65, 65, 45, 318),
(12, '-', 'Butterfree', 'Bug, Flying', 60, 45, 50, 90, 80, 70, 395),
(10, '-', 'Caterpie', 'Bug', 45, 30, 35, 20, 20, 45, 195),
(113, '-', 'Chansey', 'Normal', 250, 5, 5, 35, 105, 50, 450),
(6, '-', 'Charizard', 'Fire, Flying', 78, 84, 78, 109, 85, 100, 534),
(4, '-', 'Charmander', 'Fire', 39, 52, 43, 60, 50, 65, 309),
(5, '-', 'Charmeleon', 'Fire', 58, 64, 58, 80, 65, 80, 405),
(36, '-', 'Clefable', 'FAIRY', 95, 70, 73, 95, 90, 60, 483),
(35, '-', 'Clefairy', 'Fairy', 70, 45, 48, 60, 65, 35, 323),
(91, '-', 'Cloyster', 'Water, Ice', 50, 95, 180, 85, 45, 70, 525),
(104, '-', 'Cubone', 'Ground', 0, 50, 95, 40, 50, 35, 320),
(84, '95', 'Dduo', 'NORMAL, FLYING', 35, 85, 45, 35, 35, 75, 310),
(87, '-', 'Dewgong', 'WATER, ICE', 90, 70, 80, 70, 95, 70, 475),
(50, '', 'Diglett', 'GROUND', 10, 55, 25, 35, 45, 95, 265),
(132, '-', 'Ditto', 'NORMAL', 48, 48, 48, 48, 48, 48, 288),
(85, '96', 'Dodrio', 'NORMAL FLYING', 60, 110, 70, 60, 60, 100, 460),
(148, '-', 'Dragonair', 'Dragon', 61, 84, 65, 70, 70, 70, 420),
(149, '-', 'Dragonite', 'Dragon Flying', 91, 134, 95, 100, 100, 80, 600),
(147, '-', 'Dratini', 'Dragon', 41, 64, 45, 50, 50, 50, 300),
(96, '-', 'Drowzee', 'PSYCHIC', 60, 48, 45, 43, 90, 42, 328),
(51, '-', 'Dugtrio', 'Ground', 35, 80, 50, 50, 70, 120, 405),
(133, '-', 'Eevee', 'NORMAL', 55, 55, 50, 45, 65, 55, 325),
(23, '-', 'Ekans', 'POISON', 35, 60, 44, 40, 54, 55, 288),
(125, '-', 'Electabuzz', 'ELECTRIC', 65, 83, 57, 95, 85, 105, 490),
(101, '88', 'Electrode', 'ELECTRIC', 60, 50, 70, 80, 80, 140, 480),
(102, '-', 'Exeggcute', 'GRASS PSYCHIC', 60, 40, 80, 60, 45, 40, 325),
(83, '-', 'Farfetch’d', 'NORMAL FLYING', 52, 65, 55, 58, 62, 60, 352),
(22, '-', 'Fearow', 'NORMAL FLYING', 65, 90, 65, 61, 61, 100, 442),
(136, '-', 'Flareon', 'FIRE', 65, 130, 60, 95, 110, 65, 525),
(92, '-', 'Gastly', 'GHOST POISON', 30, 35, 30, 100, 35, 80, 310),
(94, '-', 'Gengar', 'GHOST POISON', 60, 65, 60, 130, 75, 110, 500),
(74, '58', 'Geodude', 'ROCK GROUND', 40, 80, 100, 30, 30, 20, 300),
(44, '92', 'Gloom', 'GRASS POISON', 60, 65, 70, 85, 75, 40, 395),
(42, '66', 'Golbat', 'POISON FLYING', 75, 80, 70, 65, 75, 90, 455),
(118, '51', 'Goldeen', 'WATER', 45, 67, 60, 35, 50, 63, 320),
(55, '166', 'Golduck', 'WATER', 80, 82, 78, 95, 80, 85, 500),
(76, '60', 'Golem', 'ROCK GROUND', 80, 120, 130, 55, 65, 45, 495),
(75, '59', 'Graveler', 'ROCK GROUND', 55, 95, 115, 45, 45, 35, 390),
(88, '111', 'Grimmer', 'POISON', 80, 80, 50, 40, 50, 25, 325),
(58, '-', 'Growlithe', 'FIRE', 55, 70, 45, 70, 50, 60, 350),
(130, '54', 'Gyarados', 'WATER FLYING', 95, 125, 79, 60, 100, 81, 540),
(93, '-', 'Haunter', 'GHOST POISON', 45, 50, 45, 115, 55, 95, 405),
(107, '-', 'Hitmonchan', 'FIGHT', 50, 105, 79, 35, 110, 76, 455),
(106, '-', 'Hitmonlee', 'FIGHT', 50, 120, 53, 35, 110, 87, 455),
(116, '193', 'Horsea', 'WATER', 30, 40, 70, 70, 25, 60, 295),
(97, '-', 'Hypno', 'PSYCHIC', 85, 73, 70, 73, 115, 67, 483),
(2, '-', 'Ivysaur', 'GRASS', 60, 62, 63, 80, 80, 60, 405),
(39, '143', 'Jigglypuff', 'NORMAL FAIRY', 115, 45, 20, 45, 25, 20, 270),
(135, '-', 'Jolteon', 'ELECTRIC', 65, 65, 60, 110, 95, 130, 525),
(124, '-', 'Jynx', 'ICE PSYCHIC', 65, 50, 35, 115, 95, 95, 455),
(140, '-', 'Kabuto', 'ROCK WATER', 30, 80, 90, 55, 45, 55, 355),
(141, '-', 'Kabutops', 'Rock, Water', 60, 115, 105, 65, 70, 80, 495),
(64, '41', 'Kadabra', 'PSYCHIC', 40, 35, 30, 120, 70, 105, 400),
(14, '-', 'Kakuna', 'BUG POISON', 45, 25, 50, 25, 25, 35, 205),
(115, '-', 'Kangaskhan', 'NORMAL', 105, 95, 80, 40, 80, 90, 490),
(99, '-', 'Kingler', 'WATER', 55, 130, 115, 50, 50, 75, 475),
(109, '113', 'Koffing', 'POISON', 40, 65, 95, 60, 45, 35, 340),
(98, '-', 'Krabby', 'WATER', 30, 105, 90, 25, 25, 50, 325),
(131, '-', 'Lapras', 'WATER ICE', 130, 85, 80, 85, 95, 60, 535),
(108, '-', 'Lickitung', 'NORMAL', 90, 55, 75, 60, 75, 30, 385),
(69, '77', 'Machamp', 'FIGHT', 90, 130, 80, 65, 85, 55, 505),
(68, '76', 'Machoke', 'FIGHT', 80, 100, 70, 50, 60, 45, 405),
(66, '75', 'Machop', 'FIGHT', 70, 80, 50, 35, 35, 35, 305),
(129, '53', 'Magikarp', 'WATER', 20, 10, 55, 15, 20, 80, 200),
(126, '-', 'Magmar', 'FIRE', 65, 95, 57, 100, 85, 93, 495),
(81, '84', 'Magnemite', 'ELECTRIC STEEL', 25, 35, 70, 95, 55, 45, 325),
(82, '85', 'Magneton', 'ELECTRIC STEEL', 50, 60, 95, 120, 70, 70, 465),
(56, '-', 'Mankey', 'FIGHT', 40, 80, 35, 35, 45, 70, 305),
(105, '-', 'Marowak', 'GROUND', 60, 80, 110, 50, 80, 45, 425),
(142, '-', 'Mega Aerodactyl', 'Rock Flying', 80, 135, 80, 70, 95, 150, 615),
(65, '42', 'Mega Alakazam', 'PSYCHIC', 55, 50, 65, 175, 95, 150, 590),
(15, '-', 'Mega Beedrill', 'BUG POISON', 65, 150, 40, 15, 80, 145, 495),
(6, '-', 'Mega Charizard X', 'FIRE DRAGON', 78, 120, 78, 100, 85, 100, 561),
(6, '-', 'Mega Charizard Y', 'FIRE FLYING', 78, 104, 78, 159, 115, 100, 634),
(94, '-', 'Mega Gengar', 'GHOST POISON', 60, 65, 80, 170, 95, 130, 600),
(130, '54', 'Mega Gyarados', 'WATER DARK', 95, 155, 109, 70, 130, 81, 640),
(150, '-', 'Mega Mewtwo X', 'Psychic Fight', 106, 190, 100, 154, 100, 130, 780),
(150, '-', 'Mega Mewtwo Y', 'Psychic', 106, 150, 70, 194, 120, 140, 780),
(18, '-', 'Mega Pidgeot', 'NORMAL FLYING', 83, 80, 75, 70, 70, 101, 479),
(127, '174', 'Mega Pinsir', 'BUG FLYING', 65, 155, 120, 65, 90, 105, 600),
(81, '-', 'Mega Slowbrow', 'WATER PSYCHIC', 95, 75, 180, 130, 80, 30, 590),
(3, '-', 'Mega Venusaur', 'GRASS POISON', 80, 100, 123, 122, 120, 80, 625),
(52, '-', 'Meowth', 'NORMAL', 40, 45, 35, 40, 40, 90, 290),
(11, '-', 'Metapod', 'BUG', 50, 20, 55, 25, 25, 30, 205),
(151, '-', 'Mew', 'Psychic', 100, 100, 100, 100, 100, 100, 600),
(150, '-', 'Mewtwo', 'Psychic', 106, 110, 90, 154, 90, 130, 680),
(146, '-', 'Moltres', 'Fire Flying', 90, 100, 90, 125, 85, 90, 580),
(122, '-', 'Mr.Mime', 'PSYCHIC FAIRY', 40, 45, 65, 100, 120, 90, 460),
(89, '112', 'Mulk', 'POISON', 105, 105, 75, 65, 100, 50, 500),
(34, '-', 'Nidoking', 'POISON GROUND', 81, 102, 77, 85, 75, 85, 505),
(31, '-', 'Nidoqueen', 'POISON GROUND', 90, 92, 87, 75, 85, 76, 505),
(29, '-', 'Nidoran(F)', 'POISON', 55, 47, 52, 40, 40, 41, 275),
(32, '-', 'Nidoran(M)', 'POISON', 46, 57, 40, 40, 40, 50, 273),
(30, '-', 'Nidorina', 'POISON', 70, 62, 67, 55, 55, 56, 365),
(33, '-', 'Nidorino', 'POISON', 61, 72, 57, 55, 55, 65, 365),
(38, '161', 'Ninetales', 'FIRE', 73, 76, 75, 81, 100, 100, 505),
(43, '91', 'Oddish', 'GRASS POISON', 45, 50, 55, 75, 65, 30, 320),
(138, '-', 'Omanyte', 'ROCK WATER', 35, 40, 100, 90, 55, 35, 355),
(139, '-', 'Omastar', 'ROCK WATER', 70, 60, 125, 115, 70, 55, 495),
(95, '-', 'Onix', 'ROCK GROUND', 35, 45, 160, 30, 45, 70, 385),
(46, '-', 'Paras', 'BUG GRASS', 35, 70, 55, 45, 55, 25, 285),
(47, '-', 'Parasect', 'BUG GRASS', 60, 95, 80, 60, 80, 30, 405),
(53, '-', 'Persian', 'NORMAL', 65, 70, 60, 65, 65, 115, 440),
(18, '-', 'Pidgeot', 'NORMAL FLYING', 83, 80, 75, 70, 70, 101, 479),
(17, '-', 'Pidgeotto', 'NORMAL FLYING', 63, 60, 55, 50, 50, 71, 349),
(16, '-', 'Pidgey', 'NORMAL FLYING', 40, 45, 40, 35, 35, 56, 251),
(25, '163', 'Pikachu <3', 'ELECTRIC', 35, 55, 40, 50, 50, 90, 320),
(25, '163', 'Pikachu Belle', 'ELECTRIC', 35, 55, 40, 40, 50, 90, 320),
(25, '163', 'Pikachu Cosplay', 'ELECTRIC', 55, 40, 50, 50, 50, 90, 320),
(25, '163', 'Pikachu Libre', 'ELECTRIC', 35, 55, 40, 40, 50, 90, 320),
(25, '163', 'Pikachu PHD', 'ELECTRIC', 35, 55, 40, 40, 50, 90, 320),
(25, '163', 'Pikachu Pop Start', 'ELECTRIC', 35, 55, 40, 40, 50, 90, 320),
(25, '163', 'Pikachu Rock Star', 'ELECTRIC', 35, 55, 40, 50, 50, 90, 320),
(127, '174', 'Pinsir', 'BUG', 65, 125, 100, 55, 70, 85, 500),
(60, '-', 'Poliwag', 'WATER', 40, 50, 40, 40, 40, 90, 300),
(61, '-', 'Poliwhirl', 'WATER', 65, 65, 65, 50, 50, 90, 385),
(62, '-', 'Poliwrath', 'WATER FIGHT', 90, 95, 95, 70, 90, 70, 510),
(77, '-', 'Ponyta', 'FIRE', 50, 85, 55, 65, 65, 90, 410),
(137, '-', 'Porygon', 'NORMAL', 65, 60, 70, 85, 75, 40, 395),
(57, '-', 'Primeape', 'FIGHT', 65, 105, 60, 60, 70, 95, 455),
(54, '165', 'Psyduck', 'WATER', 50, 52, 48, 65, 50, 55, 320),
(26, '164', 'Raichu', 'ELECTRIC', 60, 90, 55, 90, 80, 110, 485),
(78, '-', 'Rapidash', 'FIRE', 65, 100, 70, 80, 80, 105, 500),
(20, '-', 'Raticate', 'NORMAL', 55, 81, 60, 50, 70, 97, 413),
(19, '-', 'Rattata', 'NORMAL', 30, 56, 35, 25, 35, 72, 253),
(112, '177', 'Rhydon', 'GROUND ROCK', 105, 130, 120, 45, 45, 40, 485),
(111, '176', 'Rhyhorn', 'GROUND ROCK', 80, 85, 95, 30, 30, 25, 345),
(27, '117', 'Sandshrew', 'GROUND', 50, 75, 85, 20, 30, 40, 300),
(28, '118', 'Sandslash', 'GROUND', 75, 100, 110, 45, 55, 65, 450),
(123, '-', 'Scyther', 'BUG FLYING', 70, 110, 80, 55, 80, 105, 500),
(117, '194', 'Seadra', 'WATER', 55, 65, 95, 95, 45, 85, 440),
(119, '52', 'Seaking', 'WATER', 80, 92, 65, 65, 80, 68, 450),
(86, '-', 'Seel', 'WATER', 65, 45, 55, 45, 70, 45, 325),
(90, '-', 'Shellder', 'WATER', 30, 65, 100, 45, 25, 40, 305),
(80, '-', 'Slowbro', 'WATER PSYCHIC', 95, 75, 110, 100, 80, 30, 490),
(79, '-', 'Slowpoke', 'WATER PSYCHIC', 90, 65, 65, 40, 40, 15, 315),
(143, '-', 'Snorlax', 'Normal', 160, 110, 65, 65, 110, 30, 540),
(21, '-', 'Spearow', 'NORMAL FLYING', 40, 60, 30, 31, 31, 70, 262),
(7, '-', 'Squirtle', 'WATER', 44, 48, 65, 50, 64, 43, 314),
(121, '149', 'Starmie', 'WATER PSYCHIC', 60, 75, 85, 100, 85, 115, 520),
(120, '148', 'Staryu', 'WATER', 30, 45, 55, 70, 55, 85, 340),
(114, '-', 'Tangela', 'GRASS', 65, 55, 115, 100, 40, 60, 435),
(128, '-', 'Tauros', 'NORMAL', 75, 100, 95, 40, 70, 110, 490),
(72, '68', 'Tentacool', 'WATER POISON', 40, 40, 35, 50, 100, 70, 335),
(73, '69', 'Tentacruel', 'WATER POISON', 80, 70, 65, 80, 120, 100, 515),
(134, '-', 'Vaporeon', 'WATER', 130, 65, 60, 110, 95, 65, 525),
(49, '-', 'Venomoth', 'BUG POISON', 70, 65, 60, 90, 75, 90, 450),
(48, '-', 'Venonat', 'BUG POISON', 60, 55, 50, 40, 55, 45, 305),
(3, '-', 'Venusaur', 'GRASS POISON', 80, 82, 83, 100, 100, 80, 525),
(71, '-', 'Victreebel', 'GRASS POISON', 80, 105, 65, 100, 70, 70, 490),
(45, '93', 'Vileplume', 'GRASS POISON', 75, 80, 85, 110, 90, 50, 490),
(100, '87', 'Voltorb', 'Electric', 40, 30, 50, 55, 55, 100, 330),
(37, '160', 'Vulpix', 'FIRE', 38, 41, 40, 50, 65, 65, 299),
(8, '-', 'Wartortle', 'WATER', 59, 63, 80, 65, 80, 58, 405),
(13, '-', 'Weedle', 'BUG POISON', 40, 35, 30, 20, 20, 50, 195),
(70, '-', 'Weepinbell', 'GRASS POISON', 65, 90, 50, 85, 45, 55, 390),
(110, '114', 'Weezing', 'POISON', 65, 90, 120, 85, 70, 60, 490),
(40, '144', 'Wigglytuff', 'NORMAL FAIRY', 140, 70, 45, 85, 50, 45, 435),
(145, '-', 'Zapdos', 'Electric Flying', 90, 90, 85, 125, 90, 100, 580),
(41, '65', 'Zubat', 'POISON FLYING', 40, 45, 35, 30, 40, 55, 245);

-- --------------------------------------------------------

--
-- Table structure for table `TypeRelation`
--

CREATE TABLE `TypeRelation` (
  `Name` varchar(25) NOT NULL,
  `TypeName` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Types`
--

CREATE TABLE `Types` (
  `TypeName` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Types`
--

INSERT INTO `Types` (`TypeName`) VALUES
('Bug'),
('Dark'),
('Dragon'),
('Electric'),
('Fairy'),
('Fighting'),
('Fire'),
('Flying'),
('Ghost'),
('Grass'),
('Ground'),
('Ice'),
('Normal'),
('Poison'),
('Psychic'),
('Rock'),
('Steel'),
('Water');

-- --------------------------------------------------------

--
-- Table structure for table `Weaknesses`
--

CREATE TABLE `Weaknesses` (
  `TypeName` varchar(25) NOT NULL,
  `WeakAgainst` varchar(150) NOT NULL,
  `VulnerableTo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Weaknesses`
--

INSERT INTO `Weaknesses` (`TypeName`, `WeakAgainst`, `VulnerableTo`) VALUES
('Normal', 'Rock, Ghost, Steel', 'Fighting'),
('Fighting', 'Flying, Poison, Psychic, Bug, Ghost, Fairy', 'Flying, Psychic, Fairy'),
('Flying', 'Rock, Steel, Electric', 'Rock, Electric, Ice'),
('Poison', 'Poison, Ground, Rock, Ghost, Steel', 'Ground, Psychic'),
('Ground', 'Flying, Bug, Grass', 'Water, Grass, Ice'),
('Rock', 'Fighting, Ground, Steel', 'Fighting, Ground, Steel, Water, Grass'),
('Bug', 'Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy', 'Flying, Rock, Fire'),
('Ghost', 'Normal, Dark', 'Ghost, Dark'),
('Steel', 'Steel, Fire, Water, Electric', 'Fighting, Ground, Fire'),
('Fire', 'Rock, Fire, Water, Dragon', 'Ground, Rock, Water'),
('Water', 'Water, Grass, Dragon', 'Grass, Electric'),
('Grass', 'Flying, Poison, Bug, Steel, Fire, Grass, Dragon', 'Flying, Poison, Bug, Fire, Ice'),
('Electric', 'Ground, Grass, Electric, Dragon', 'Ground'),
('Psychic', 'Steel, Psychic, Dark', 'Bug, Ghost, Dark'),
('Ice', 'Steel, Fire, Water, Ice', 'Fighting, Rock, Steel, Fire'),
('Dragon', 'Steel, Fairy', 'Ice, Dragon, Fairy'),
('Fairy', 'Poison, Steel, Fire', 'Poison, Steel'),
('Dark', 'Fighting, Dark, Fairy', 'Fighting, Bug, Fairy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Effectiveness`
--
ALTER TABLE `Effectiveness`
  ADD KEY `TypeName` (`TypeName`);

--
-- Indexes for table `Pokemon`
--
ALTER TABLE `Pokemon`
  ADD PRIMARY KEY (`Name`);

--
-- Indexes for table `TypeRelation`
--
ALTER TABLE `TypeRelation`
  ADD PRIMARY KEY (`Name`,`TypeName`),
  ADD KEY `TypeName` (`TypeName`);

--
-- Indexes for table `Types`
--
ALTER TABLE `Types`
  ADD PRIMARY KEY (`TypeName`);

--
-- Indexes for table `Weaknesses`
--
ALTER TABLE `Weaknesses`
  ADD KEY `TypeName` (`TypeName`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Effectiveness`
--
ALTER TABLE `Effectiveness`
  ADD CONSTRAINT `Effectiveness_ibfk_1` FOREIGN KEY (`TypeName`) REFERENCES `Types` (`TypeName`);

--
-- Constraints for table `TypeRelation`
--
ALTER TABLE `TypeRelation`
  ADD CONSTRAINT `TypeRelation_ibfk_1` FOREIGN KEY (`Name`) REFERENCES `Pokemon` (`Name`),
  ADD CONSTRAINT `TypeRelation_ibfk_2` FOREIGN KEY (`TypeName`) REFERENCES `Types` (`TypeName`);

--
-- Constraints for table `Weaknesses`
--
ALTER TABLE `Weaknesses`
  ADD CONSTRAINT `Weaknesses_ibfk_1` FOREIGN KEY (`TypeName`) REFERENCES `Types` (`TypeName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
