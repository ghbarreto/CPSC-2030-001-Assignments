<?php
   require_once 'sqlhelper.php';
   require_once './vendor/autoload.php';  
   $loader = new Twig_Loader_Filesystem('./templates'); 
    $twig = new Twig_Environment($loader);

   $conn = connectToMyDatabase();
   $result = $conn->query("call pokemon_all");
   $populars = $conn -> query ("call pokemon_popular");

   if($result){
      $table = $result->fetch_all(MYSQLI_ASSOC); 
      $populars = $result->fetch_all(MYSQLI_ASSOC); 
      
      $template = $twig->load('frontpage.twig.html');

      echo $template -> render(array("pokemons" => $table,
      "title" => "Welcome to the pokemon Website",
      "favourites" => $populars));
      
      $conn->close(); 
   }

?>
