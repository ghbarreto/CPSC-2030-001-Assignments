<?php

function clearConnection($mysql){
    while($mysql->more_results()){
       $mysql->next_result();
       $mysql->use_result();
    }
}
function wrap($tag,$value) { 
    return "<$tag>$value</$tag>";
}

function connectToMyDatabase(){
    $user = 'ghbarreto';
    $pwd = '';
    $server = 'localhost';
    $dbname = 'MyQueries';
 

 
    $conn = new mysqli($server, $user, $pwd, $dbname);
    return $conn;   
}
function setupMyTwigEnvironment(){
    $loader = new Twig_Loader_Filesystem('./templates'); 
    $twig = new Twig_Environment($loader); 
    return $twig;  
}

function dumpErrorPage($twig){
    $template = $twig->load("error.twig.html");
    echo $template->render(array("message"=>"SQL errorm query failed"));
}
?>

