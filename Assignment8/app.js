$('document').ready(function (){


    // menu click expand event

    $('#menuBt').on('click', (e)=> {
        if( $('#list ul li').css('width') == '0px')
            menuStart();
        else
            menuStop();
        // console.log( $('#list #menuBt').css('transform'));

    });

    // menu items click event

    $('#list ul li a').on('click', (e)=>{
        e = e || window.event;

        e.preventDefault();
        // get mouseclick
        var pageX = e.pageX;
        var pageY = e.pageY;


        shootCannonBall(pageX, pageY);

    });




    // menu animation init
    function menuStart(){
        //extend menu
            $('#list ul li').css('width', '100%');
            //rotate the cannon
            $('#list #menuBt').css({
                'transform' : 'rotate(-30deg)',
                'transform-origin':' 0% 30%'
            });
            // place cannon ball
             $('#cannonBall').css('transform','translate(250%,-200%)');
    }

    function menuStop(){
        $('#list ul li').css('width', '0px');
        $('#list #menuBt').css({
            'transform' : 'rotate(0deg)',
            'transform-origin':' 0% 30%'
        });
        $('#cannonBall').css('transform','translate(0%,0%)');
    }


    function shootCannonBall( x, y ){
        console.log('translate('+ x +',' + y + ')');

        //$('#cannonBall').addClass('box');
        $('#cannonBall').css('transform','translate('+ (x-12) +'px,' + (y-176) + 'px)');



    }




});
