<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 

if(!$_SESSION['user']){
    header('Location: ../html/log-in.php');
    exit();
}