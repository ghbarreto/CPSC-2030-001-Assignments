<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <link rel="stylesheet/less" type="text/css" media="screen" href="../style/menu.less" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <script>

    </script>
</head>
<body>
<a href="proposal/proposal.html" id="proposal">PROPOSAL</a>
<img src="../images/new1.jpg" alt="Welcome">

<nav id="dropdown">
    <div>
        <i class="fas fa-bars"></i>
    </div>
</nav>
<nav id = "single-nav" role="navigation">
    <ul>
        <li>
            <a href="../home.php">
                <div class="icon">
                    <i class="fa fa-home"></i>
                </div>
                <div class ="bt">Home</div>
            </a>

        </li>
        <li>
            <a href="../html/contact.php">
                <div class="icon">
                    <i class="fas fa-phone-square"></i>
                </div>
                <div class ="bt">Contact</div>
            </a>
        </li>
        <li>
            <a href="../html/jobs.php">
                <div class="icon">
                    <i class="far fa-building"></i>
                </div>
                <div class ="bt">Jobs</div>
            </a>
        </li>
        <li><a href="../html/userpanel.php">
            <div class="icon">
                <i class="far fa-user"></i>
            </div>
            <div class ="bt">Log-In</div>
        </a>
        </li>
    </ul>
</nav>

<nav class="welcome">
    <h1>Contact Information</h1>
    <p> To get in contact with any of our hitmen you have to submit an application <br>
        through this form underneath, you should provide every details for <br>
        better support.
        <br>
        <br>
        <form action="php">
            <div class="a"> 
            <input type="text" name="inputBox" placeholder="Name" value="<?php if($_SESSION){ echo $_SESSION['user'];}?>">
            </div>
            <div class="a">
                 <input type="text" name="inputBox" placeholder="E-Mail">
            </div>
            
        <textarea rows="4" cols="50" >
            </textarea>
            <input type="submit">
        </form>
        <br>
        <br>
    </p>
</nav>

    </p>
</nav>

</body>
<script src ="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="../scripts/menu.js"></script>
</html>
