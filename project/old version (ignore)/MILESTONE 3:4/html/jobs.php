<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <link rel="stylesheet/less" type="text/css" media="screen" href="../style/menu.less" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <script>

    </script>
</head>
<body>
<a href="proposal/proposal.html" id="proposal">PROPOSAL</a>
<img src="../images/new1.jpg" alt="Welcome">

<nav id="dropdown">
    <div>
        <i class="fas fa-bars"></i>
    </div>
</nav>
<nav id = "single-nav" role="navigation">
    <ul>
        <li>
            <a href="../home.php">
                <div class="icon">
                    <i class="fa fa-home"></i>
                </div>
                <div class ="bt">Home</div>
            </a>

        </li>
        <li>
            <a href="../html/contact.php">
                <div class="icon">
                    <i class="fas fa-phone-square"></i>
                </div>
                <div class ="bt">Contact</div>
            </a>
        </li>
        <li>
            <a href="../html/jobs.php">
                <div class="icon">
                    <i class="far fa-building"></i>
                </div>
                <div class ="bt">Jobs</div>
            </a>
        </li>
        <li><a href="../html/userpanel.php">
            <div class="icon">
                <i class="far fa-user"></i>
            </div>
            <div class ="bt">Log-In</div>
        </a>
        </li>
    </ul>
</nav>

<nav class="welcome">
    <h1>Choose your product<?php if($_SESSION){ echo ", ".$_SESSION['user']; } else{echo" ";}?>!</h1>
    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent  <br>
        efficitur ex sed euismod pretium. Morbi facilisis, magna nec <br>
        maximus gravida, risus magna elementum nisl, et cursus ex velit non <br>
        leo. Integer blandit ligula sit amet elementum accumsan. Integer <br>
        ultrices lorem sed augue bibendum, et bibendum nunc luctus. Nunc <br>
        at mauris vehicula, rhoncus ante in, bibendum felis. Mauris pulvinar<br>
        ut ligula vitae aliquam. Etiam viverra felis ut feugiat luctus. Etiam <br>
        convallis erat id bibendum posuere. <br>
    </p>
</nav>


</body>

<script src ="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="../scripts/menu.js"></script>
</html>
