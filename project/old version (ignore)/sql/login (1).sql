-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 26, 2018 at 05:18 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_products`
--

CREATE TABLE `cart_products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_description` text NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `product_quantity` int(99) NOT NULL,
  `product_img` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_products`
--

INSERT INTO `cart_products` (`product_id`, `product_name`, `product_description`, `product_price`, `product_quantity`, `product_img`) VALUES
(3, 'Murder', 'We will murder someone of your choice, just name it and pay.', '899.00', 99, 'murder.jpeg'),
(4, 'Arson', 'We will set something on fire for you.', '299.00', 99, 'fire.jpeg'),
(5, 'Beat', 'We will beat someone up until you tell us to stop.', '59.00', 99, 'punch.jpg'),
(9, 'Manslaughter', 'We will kill someone and make it looks like it was an accident.', '990.00', 99, 'manslaughter.jpg'),
(10, 'Mayhem', 'We will cut off a body part of your choice.', '499.99', 99, 'mayhem.jpg'),
(11, 'Kidnapping', 'We will kidnap someone for you.', '499.99', 99, 'kidnapping.png'),
(12, 'Robbery', 'We will rob someone for you, we won\'t rob police officers, banks or big organizations.', '299.99', 99, 'robbery.jpg'),
(13, 'Trespass', 'We will invade someone\'s house for you.', '69.99', 99, 'trespass.jpg'),
(14, 'Mobbing', 'We will bully someone for you.', '39.99', 99, 'mobbing.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `name` varchar(30) NOT NULL,
  `reason` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`name`, `reason`, `email`, `id`) VALUES
('eae', 'qweqwe@hotmail.com', 'ae eqweqweqwe', 1);

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `name` varchar(30) COLLATE utf8_swedish_ci NOT NULL,
  `feedback` text COLLATE utf8_swedish_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`name`, `feedback`, `id`) VALUES
('eae', ' blz?', 8),
('admin', 'qwejoqiwjeoqwjeoijoiqjwe', 3),
('eaeea', ' qweqwe', 4),
('qweqwe', ' qweqe1', 5),
('qweqwe', ' qweqe1', 6);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(15) NOT NULL,
  `user` varchar(30) NOT NULL,
  `money` double(6,2) DEFAULT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user`, `money`, `password`) VALUES
(130, 'admin', NULL, '21232f297a57a5a743894a0e4a801fc3'),
(131, 'john', NULL, 'a2e63ee01401aaeca78be023dfbb8c59'),
(132, 'lalala', NULL, '3b79c04ca94351cb6754731f0d37fcfe'),
(133, 'ras', NULL, '1c28067048213d48f168efc6d6685406'),
(134, 'gabriel', NULL, '647431b5ca55b04fdf3c2fce31ef1915'),
(135, '123', NULL, '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_products`
--
ALTER TABLE `cart_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user` (`user`),
  ADD UNIQUE KEY `user_2` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart_products`
--
ALTER TABLE `cart_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
