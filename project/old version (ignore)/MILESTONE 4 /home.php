<?php
session_start(); 
include('php/connection.php');

    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <link rel="stylesheet/less" type="text/css" media="screen" href="style/menu.less" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <script src ="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="scripts/menu.js"></script>

</head>
<body>
<a href="proposal/proposal.html" id="proposal">PROPOSAL</a>
<img src="images/new1.jpg" alt="Welcome">

<div class="container" id="arrows">
  <div class="bar1"></div>
  <div class="bar2"></div>
  <div class="bar3"></div>
</div>

<nav id = "single-nav" role="navigation">
    <ul>
        <li>
            <a href="home.php">
            <div class="icon">
                <i class="fa fa-home"></i>
            </div>
                <div class ="bt">Home</div>
            </a>
        </li>
        <li>
            <a href="html/about.php">
            <div class="icon">
                <i class="fa fa-home"></i>
            </div>
                <div class ="bt">About</div>
            </a>
        </li>

        <li>
            <a href="html/contact.php">
                <div class="icon">
                    <i class="fas fa-phone-square"></i>
                </div>
                <div class ="bt">Contact</div>
            </a>
        </li>
        <li>
            <a href="html/jobs.php">
                <div class="icon">
                    <i class="far fa-building"></i>
                </div>
                <div class ="bt">Jobs</div>
            </a>
        </li>
        <li><a href="html/userpanel.php">
            <div class="icon">
                <i class="far fa-user"></i>
            </div>
            <div class ="bt">User Panel</div>
        </a>
        </li>
    </ul>
</nav>

<nav class="welcome">
    <h1>Welcome <?php if($_SESSION){ echo $_SESSION['user']; } else{echo" ";}?> !</h1>
        <p> Welcome to the website, if you are here, then it means you have someone  
            you want to be wiped off of the face of the Earth, if thats the case 
            then you are in the right place, we will take care of that for you 
            for the right price, thank you for choosing us! <br><br>
               
        </p>
</nav>

<nav class="welcome">
    <h1>News !</h1>
    <p> Hello everyone, this is the Administrator, <br>
        Good news coming your way, everything in the <br>
        store is 50% off! so you can order to kill 2 people <br>
        and pay only the price of 1, don't waste your time and <br>
        <br>
        <a class="aHovers" href="html/jobs.php">ORDER IT ALREADY!</a></span>
        <br><br>
    </p>
</nav>

<nav class="welcome">
    <h1>Feedback!</h1>
    
    <p>If you want to leave a comment, please log onto your account
     and your comment will be showing up below</p>

     <p> or you can be redirected by clicking <a class="aHovers" href="html/feedback.php"> right here. </a> 
<br><br>
</nav>

<nav class="welcome">
<h1> Our Feedbacks ! </h1> <br> <hr>
<?php
    $run = "CALL runFeedbacks()"; // run the sql
    $feed = mysqli_query($conn, $run); // scan the sql
    $feedbacks = mysqli_num_rows($feed); // get the sql rows

    if($feedbacks>0){ // print all the feedbacks
        while($a = mysqli_fetch_array($feed)){
            $name = $a ['name'];
            $comments = $a ['feedback'];
            echo "<br />";
            echo "" . $name;
            echo "<br />";

            echo "" . $comments;
            echo "<br />";
            echo "<br />";
            echo "<hr>";
        }
    }
?>
<br>
</nav>

</body>


</html>
