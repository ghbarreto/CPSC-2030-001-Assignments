<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <link rel="stylesheet/less" type="text/css" media="screen" href="../style/menu.less" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <script>

    </script>
</head>
<body>
<a href="proposal/proposal.html" id="proposal">PROPOSAL</a>

<div class="container" id="arrows">
  <div class="bar1"></div>
  <div class="bar2"></div>
  <div class="bar3"></div>
</div>
<nav id = "single-nav" role="navigation">
    <ul>
        <li>
            <a href="../home.php">
                <div class="icon">
                    <i class="fa fa-home"></i>
                </div>
                <div class ="bt">Home</div>
            </a>

        </li>
        <li>
            <a href="../html/about.php">
            <div class="icon">
                <i class="fa fa-home"></i>
            </div>
                <div class ="bt">About</div>
            </a>
        </li>
        <li>
            <a href="../html/contact.php">
                <div class="icon">
                    <i class="fas fa-phone-square"></i>
                </div>
                <div class ="bt">Contact</div>
            </a>
        </li>
        <li>
            <a href="../html/jobs.php">
                <div class="icon">
                    <i class="far fa-building"></i>
                </div>
                <div class ="bt">Jobs</div>
            </a>
        </li>
        <li><a href="../html/userpanel.php">
            <div class="icon">
                <i class="far fa-user"></i>
            </div>
            <div class ="bt">Log-In</div>
        </a>
        </li>
    </ul>
</nav>

<nav class="welcome">
    <h1>About</h1>
    <p> Hello, we are a group of hitmen and we are very much known for our quick <br>
    and clean job in our services. We are not messy and we make sure that our targets <br>
    stay down. We also provide 100% security, so no one will ever know about whoever <br>
    ordered the service, even if one of us go down. If you need any help you can ask <br>
    <a class="aHovers" href="contact.php"> here</a> and we will answer you as fast as we can with the <br>
    best support available within hitmen websites.<br><br>
    </p>
</nav>

    </p>
</nav>
<script src ="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="../scripts/menu.js"></script>

</html>
