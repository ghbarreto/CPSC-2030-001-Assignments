<?php
include('connection.php');

// if user doesn't fill name or email it doesn't run the sql
if (isset($_POST['name']) && isset($_POST['email']) && ($_POST['name'] == '' || $_POST['email'] == '')) {
    echo 'please fill all the fields';
    exit();
}

// if reason is not filled, does not run the sql
if (isset($_POST['reason']) || ($_POST['reason'] == '')) {
    echo 'please fill all the fields';
    exit();
}

// assign the variables to the values the user filled in the forms
$contactName = $_POST['name'];
$email = $_POST['email'];
$reason = $_POST['reason'];

// calling the insert procedure
$run = "CALL contactSend('$contactName', '$email', '$reason')";

// sending a message that the content was inserted.
if ($conn->query($run) === true) {
    echo "Thank you for contacting us";
} else {
    echo "Error: " . $run . "<br>" . $conn->error;
}
?>