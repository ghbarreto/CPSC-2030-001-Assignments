-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 04-Dez-2018 às 04:04
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `AverageReviews` ()  SELECT AVG (ratings) as avg FROM feedbacks$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `checkAvailability` (IN `userCheck` INT(30), IN `passwordCheck` VARCHAR(32))  select user_id, user FROM user where user = userCheck and password = md5(passwordCheck)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `checkUser` (IN `userAlready` VARCHAR(32))  SELECT user
from user 
where user = userAlready$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `contactSend` (IN `contactName` VARCHAR(30), IN `emailUser` VARCHAR(50), IN `reasonUser` TEXT)  INSERT INTO contact(name,email ,reason) VALUES(contactName, emailUser, reasonUser)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `createFeedback` (IN `nameInput` VARCHAR(30), IN `feedbackInput` TEXT, IN `ratingInput` INT(1))  INSERT INTO feedbacks(name,feedback, ratings) VALUES(nameInput, feedbackInput, ratingInput)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `loginCreation` (IN `userName` VARCHAR(30), IN `passName` VARCHAR(32))  Insert into user(user, password)
values (userName, md5(passName))$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `runFeedbacks` ()  SELECT * FROM feedbacks 
ORDER BY id DESC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectCart` ()  NO SQL
SELECT * FROM cart_products$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectProduct` (IN `idProduct` INT)  SELECT * FROM cart_products 
WHERE product_id = idProduct$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_products`
--

CREATE TABLE `cart_products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_description` text NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `product_quantity` int(99) NOT NULL,
  `product_img` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cart_products`
--

INSERT INTO `cart_products` (`product_id`, `product_name`, `product_description`, `product_price`, `product_quantity`, `product_img`) VALUES
(3, 'Murder', 'We will murder someone of your choice, just name it and pay.', '899.00', 99, 'murder.jpeg'),
(4, 'Arson', 'We will set something on fire for you.', '299.00', 99, 'fire.jpeg'),
(5, 'Beat', 'We will beat someone up until you tell us to stop.', '59.00', 99, 'punch.jpg'),
(9, 'Manslaughter', 'We will kill someone and make it looks like it was an accident.', '990.00', 99, 'manslaughter.jpg'),
(10, 'Mayhem', 'We will cut off a body part of your choice.', '499.99', 99, 'mayhem.jpg'),
(11, 'Kidnapping', 'We will kidnap someone for you.', '499.99', 99, 'kidnapping.png'),
(12, 'Robbery', 'We will rob someone for you, we won\'t rob police officers, banks or big organizations.', '299.99', 99, 'robbery.jpg'),
(13, 'Trespass', 'We will invade someone\'s house for you.', '69.99', 99, 'trespass.jpg'),
(14, 'Mobbing', 'We will bully someone for you.', '39.99', 99, 'mobbing.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contact`
--

CREATE TABLE `contact` (
  `name` varchar(30) NOT NULL,
  `reason` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contact`
--

INSERT INTO `contact` (`name`, `reason`, `email`, `id`) VALUES
('Test', 'This is a test mail. ', 'Test@hotmail.com', 22);

-- --------------------------------------------------------

--
-- Estrutura da tabela `feedbacks`
--

CREATE TABLE `feedbacks` (
  `name` varchar(30) COLLATE utf8_swedish_ci NOT NULL,
  `feedback` text COLLATE utf8_swedish_ci NOT NULL,
  `id` int(11) NOT NULL,
  `ratings` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Extraindo dados da tabela `feedbacks`
--

INSERT INTO `feedbacks` (`name`, `feedback`, `id`, `ratings`) VALUES
('Gabriel Henrique', 'Hello, welcome to my website! I hope you enjoy it! :D ', 97, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `user_id` int(15) NOT NULL,
  `user` varchar(30) NOT NULL,
  `money` double(6,2) DEFAULT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`user_id`, `user`, `money`, `password`) VALUES
(154, 'gabriel', NULL, '647431b5ca55b04fdf3c2fce31ef1915'),
(155, 'admin', NULL, '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_products`
--
ALTER TABLE `cart_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user` (`user`),
  ADD UNIQUE KEY `user_2` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart_products`
--
ALTER TABLE `cart_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
