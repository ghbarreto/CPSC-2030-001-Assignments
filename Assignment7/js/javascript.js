let names = new Array();

function addSquad(){
    for (let k = 0; k <= names.length; k++){
        document.getElementById('NameSquad').innerHTML = names;
    }
}

function onClickWallace() {

    let claudeWallace = {
        Img: "images/ClaudeWallace.png",
        Name: "Claude Wallace",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "First Lieutenant",
        Role: "Tank Commander",
        Class: "Scout"
    };

    document.getElementById('Name').innerHTML = claudeWallace.Name;
    document.getElementById('Image').innerHTML = `<img src="` + claudeWallace.Img + '"/>';
    document.getElementById('Side').innerHTML = claudeWallace.Side;
    document.getElementById('Unit').innerHTML = claudeWallace.Unit;
    document.getElementById('Rank').innerHTML = claudeWallace.Rank;
    document.getElementById('Role').innerHTML = claudeWallace.Role;
    document.getElementById('Class').innerHTML = claudeWallace.Class;

    return names.push(claudeWallace.Name);
    
}

function onClickAngelica() {

    let angelicaFarnaby = {
        Img: "images/AngelicaFarnaby.png",
        Name: "Angelica Farnaby",
        Side: "Edinburgh Army",
        Unit: "Squad E",
        Rank: "Friend",
        Role: "Medic",
        Class: "Medic"
    };

    document.getElementById('Name').innerHTML = angelicaFarnaby.Name;
    document.getElementById('Image').innerHTML = `<img src="` + angelicaFarnaby.Img + '"/>';
    document.getElementById('Side').innerHTML = angelicaFarnaby.Side;
    document.getElementById('Unit').innerHTML = angelicaFarnaby.Unit;
    document.getElementById('Rank').innerHTML = angelicaFarnaby.Rank;
    document.getElementById('Role').innerHTML = angelicaFarnaby.Role;
    document.getElementById('Class').innerHTML = angelicaFarnaby.Class;

    return names.push(angelicaFarnaby.Name);
  
}

function onClickKai() {

    let kaiSchulen = {
        Img: src = "images/KaiSchulen.png",
        Name: "Kai Schulen",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Sergeant Major",
        Role: "Fireteam Leader",
        Class: "Sniper"
    };

    document.getElementById('Name').innerHTML = kaiSchulen.Name;
    document.getElementById('Image').innerHTML = `<img src="` + kaiSchulen.Img + '"/>';
    document.getElementById('Side').innerHTML = kaiSchulen.Side;
    document.getElementById('Unit').innerHTML = kaiSchulen.Unit;
    document.getElementById('Rank').innerHTML = kaiSchulen.Rank;
    document.getElementById('Role').innerHTML = kaiSchulen.Role;
    document.getElementById('Class').innerHTML = kaiSchulen.Class;

    return names.push(kaiSchulen.Name);

}

function onClickKaren() {

    let karenStuart = {
        Img: "images/KarenStuart.png",
        Name: "Karen Stuart",
        Side: "Edinburgh Army",
        Unit: "Squad E",
        Rank: "Corporal",
        Role: "Combat EMT",
        Class: "Medic"
    };

    document.getElementById('Name').innerHTML = karenStuart.Name;
    document.getElementById('Image').innerHTML = `<img src="` + karenStuart.Img + '"/>';
    document.getElementById('Side').innerHTML = karenStuart.Side;
    document.getElementById('Unit').innerHTML = karenStuart.Unit;
    document.getElementById('Rank').innerHTML = karenStuart.Rank;
    document.getElementById('Role').innerHTML = karenStuart.Role;
    document.getElementById('Class').innerHTML = karenStuart.Class;

    return names.push(karenStuart.Name);
}

function onClickLouffe() {
  
    let louffe = {
        Img: "images/Louffe.png",
        Name: "Louffe",
        Side: "Edinburgh Army",
        Unit: "Centurion, Cygnus Fleet",
        Rank: "Sargeant",
        Role: "Radar Operator",
        Class: "Grenadier"
    };

    document.getElementById('Name').innerHTML = louffe.Name;
    document.getElementById('Image').innerHTML = `<img src="` + louffe.Img + '"/>';
    document.getElementById('Side').innerHTML = louffe.Side;
    document.getElementById('Unit').innerHTML = louffe.Unit;
    document.getElementById('Rank').innerHTML = louffe.Rank;
    document.getElementById('Role').innerHTML = louffe.Role;
    document.getElementById('Class').innerHTML = louffe.Class;

    return names.push(louffe.Name);
}

function onClickMarie() {
   
    let marieBennet = {
        Img: "images/MarieBennett.png",
        Name: "Marie Bennet",
        Side: "Edinburgh Army",
        Unit: "Centurion, Cygnus Fleet",
        Rank: "Petty Officer",
        Role: "Chief of Operations",
        Class: "Veteran"
    };

    document.getElementById('Name').innerHTML = marieBennet.Name;
    document.getElementById('Image').innerHTML = `<img src="` + marieBennet.Img + '"/>';
    document.getElementById('Side').innerHTML = marieBennet.Side;
    document.getElementById('Unit').innerHTML = marieBennet.Unit;
    document.getElementById('Rank').innerHTML = marieBennet.Rank;
    document.getElementById('Role').innerHTML = marieBennet.Role;
    document.getElementById('Class').innerHTML = marieBennet.Class;

    return names.push(marieBennet.Name);

}

function onClickMiles() {
  
    let milesArbeck = {
        Img: "images/MilesArbeck.png",
        Name: "Miles Arbeck",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Sergeant",
        Role: "Tank Operator",
        Class: "Driver"
    };

    document.getElementById('Name').innerHTML = milesArbeck.Name;
    document.getElementById('Image').innerHTML = `<img src="` + milesArbeck.Img + '"/>';
    document.getElementById('Side').innerHTML = milesArbeck.Side;
    document.getElementById('Unit').innerHTML = milesArbeck.Unit;
    document.getElementById('Rank').innerHTML = milesArbeck.Rank;
    document.getElementById('Role').innerHTML = milesArbeck.Role;
    document.getElementById('Class').innerHTML = milesArbeck.Class;

    return names.push(milesArbeck.Name);
}
function onClickMinerva() {
   
    let minervaVictor = {
        Img: "images/MinervaVictor.png",
        Name: "Minerva Victor",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad F",
        Rank: "First Lieutenant",
        Role: "Senior Commander",
        Class: "Scout"
    };

    document.getElementById('Name').innerHTML = minervaVictor.Name;
    document.getElementById('Image').innerHTML = `<img src="` + minervaVictor.Img + '"/>';
    document.getElementById('Side').innerHTML = minervaVictor.Side;
    document.getElementById('Unit').innerHTML = minervaVictor.Unit;
    document.getElementById('Rank').innerHTML = minervaVictor.Rank;
    document.getElementById('Role').innerHTML = minervaVictor.Role;
    document.getElementById('Class').innerHTML = minervaVictor.Class;

    return names.push(minervaVictor.Name);

}
function onClickRagnarok() {
  
    let ragnarok = {
        Img: "images/Ragnarok.png",
        Name: "Ragnarok",
        Side: "Edinburgh Army",
        Unit: "Squad E",
        Rank: "K-9 Unit",
        Role: "Mascot",
        Class: "Medic"
    };

    document.getElementById('Name').innerHTML = ragnarok.Name;
    document.getElementById('Image').innerHTML = `<img src="` + ragnarok.Img + '"/>';
    document.getElementById('Side').innerHTML = ragnarok.Side;
    document.getElementById('Unit').innerHTML = ragnarok.Unit;
    document.getElementById('Rank').innerHTML = ragnarok.Rank;
    document.getElementById('Role').innerHTML = ragnarok.Role;
    document.getElementById('Class').innerHTML = ragnarok.Class;

    return names.push(ragnarok.Name);
}
function onClickRiley() {
  
      let rileyMiller = {
        Img: "images/RileyMiller.png",
        Name: "Riley Miller",
        Side: "Edinburgh Army",
        Unit: "Federate Joint Ops",
        Rank: "Second Lieutenant",
        Role: "Artillery Advisor",
        Class: "Grenadier"
    };

    document.getElementById('Name').innerHTML = rileyMiller.Name;
    document.getElementById('Image').innerHTML = `<img src="` + rileyMiller.Img + '"/>';
    document.getElementById('Side').innerHTML = rileyMiller.Side;
    document.getElementById('Unit').innerHTML = rileyMiller.Unit;
    document.getElementById('Rank').innerHTML = rileyMiller.Rank;
    document.getElementById('Role').innerHTML = rileyMiller.Role;
    document.getElementById('Class').innerHTML = rileyMiller.Class;
    
    return names.push(rileyMiller.Name);
}






